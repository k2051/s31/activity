// Contains all of the endpoints of our application

// We need to use express' Router() function to achieve this
const express=require('express')
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router=express.Router()

// the 'taskController' allows us to use the function defined in the taskController.js file
const taskController=require('../controllers/taskController')

// route to get all the tasks

router.get('/',(req,res)=>{
	taskController.getAllTasks().then(resultFromController=>res.send(resultFromController))
})

// route to create a task
router.post('/',(req,res)=>{
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
	
})

// route to delete a task
router.delete('/:id',(req,res)=>{
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})

// route to update a task
router.put('/:id',(req,res)=>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController=>res.send(resultFromController))
})


// Activity

// route to get specific task
router.get('/:id',(req,res)=>{
	taskController.getTask(req.params.id).then(resultFromController=>res.send(resultFromController))
})

// route to update status of a task
router.put('/:id/complete',(req,res)=>{
	taskController.updateStatus(req.params.id).then(resultFromController=>res.send(resultFromController))
})
// use "module.exports" to export the router object to use in the index.js
module.exports=router