// controllers contain the function and business logic of our Express JS application

const Task = require('../models/task')

// controller function for getting all the tasks

// defines the functions to be used in the 'taskRoute.js' file and exports these functions 
module.exports.getAllTasks= () =>{
	// model.method
	return Task.find({}).then(result=>{
		return result
	})
}

// controller function for creating task

// the request body coming from the client was passed from the 'taskRoute.js' file via the 'req.body' as an argument and is renamed as "requestBody" paramater
module.exports.createTask=(requestBody)=>{
	let newTask=new Task({
		name:requestBody.name
	})
// saves the newly created 'newTask' object in our MongoDB database
// 'then' method waits until the task is stored in the database or an error is encountered before returning a 'true' or 'false' value back to the client
// 'then' method will accept 2 arguments
	// first argument will store the result returned by the mongoose 'save' method
	// second parameter will store the 'error' object
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

// controller function for deleting tasks
module.exports.deleteTask=(taskId)=>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err)=>{
		if(err){
			console.log(err)
			return false
		} else{
			return removedTask
		}
	})
}

// controller function for updating tasks
module.exports.updateTask=(taskId,newContent)=>{
	return Task.findById(taskId).then((result,error)=>{
		if (error) {
			console.log(error)
			return false
		} 

		result.name=newContent.name
		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			} else{
				return updatedTask
			}
		})
	})
}

// controller function for getting specific task
module.exports.getTask= (taskId) =>{
	return Task.findById(taskId).then(result=>{
		return result
	})
}

// controller function for updating status of task to 'complete'
module.exports.updateStatus=(taskId)=>{
	return Task.findById(taskId).then((result,error)=>{
		if (error) {
			console.log(error)
			return false
		} 
		
		result.status='complete'
		return result.save().then((updatedStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			} else{
				return updatedStatus
			}
		})
	})
}
