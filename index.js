// Setup dependencies
const express = require('express')
const mongoose = require('mongoose')
// this will allow us to use all the routes defined in the 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')
// server setup
const app = express()
const port = 4000

app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Database Connection
mongoose.connect('mongodb+srv://admin:admin123@course-booking.flw5f.mongodb.net/B157_to-do?retryWrites=true&w=majority',
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
})

let db = mongoose.connection
// if connection error occured, output error message in console
db.on('error', console.error.bind(console,'Connection Error'))
// if connection is successful, output message in console
db.once('open',()=>console.log("We're connected to the cloud database"))

// add the task route
// allow all the task routes created in the 'taskRoute.js' file to use the '/tasks' route
app.use('/tasks', taskRoute)
// http:localhost:4000/tasks/



app.listen(port,()=>{console.log(`Server is running at port ${port}`)})